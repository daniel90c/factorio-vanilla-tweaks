data:extend(
{
{
    type = "item",
    name = "wooden-pallet",
    icon = "__VanillaTweaks__/graphics/item/wooden-pallet.png",
    icon_size = 32,
    --flags = {"goes-to-quickbar"},
    fuel_category = "chemical",
    fuel_value = "4MJ",
    subgroup = "storage",
    order = "a[items]-a[1-pallet]",
    place_result = "wooden-pallet",
    stack_size = 50
  }
}
)