data:extend(
{
{
    type = "item",
    name = "iron-pallet",
    icon = "__VanillaTweaks__/graphics/item/iron-pallet.png",
    icon_size = 32,
    --flags = {"goes-to-quickbar"},
    fuel_category = "chemical",
    fuel_value = "0MJ",
    subgroup = "storage",
    order = "a[items]-a[2-pallet]",
    place_result = "iron-pallet",
    stack_size = 50
  }
}
)